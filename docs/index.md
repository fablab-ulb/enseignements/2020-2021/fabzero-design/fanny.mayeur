## A propos de moi

![](docs/images/profil.jpeg) 

Hello hello! Je m'appelle Fanny Mayeur. Je suis étudiante à l'ULB en architecture en Master2. Pour ma dernière année à la fac (si tout se passe bien évidemment, on croise les doigts!), j'ai choisi de suivre l'option Design donnée au Gablab de l'ULB. Ce choix s'est assez vite imposé car il me permettait de m'ouvrir vers un nouveau champ de compétences, qui jusque là restait vierge. Grâce à ma page sur ce site, vous pourrez suivre en détail l'évolution de mon apprentissage, mes échecs, mes réussite et toutes les solutions par lesquelles je serai passer pour comprendre comment mieux avancer. Alors n'hésitez pas à y jeter un petit coup d'oeil ;) 


## Et si je vous en disais un peu plus sur moi...

Je suis née et j'ai grandi ici à Bruxelles mais ça ne m'a pas pour autant empêché d'élargir mes horizons et de partir vivre pendant près d'un an et demi à l'étranger. J'ai eu l'occasion, en sortant de secondaires, de faire une année d'échange aux USA en Pennsylvanie. J'ai vécu dans deux familles d'accueil qui sont devenues pour moi membres à part entière de ma vie. Grâce à cette année j'ai pu découvrir une nouvelle manière de vivre, une nouvelle culture et surtout me rendre compte à quel point le monde offre des possibilités diverses et variées lorsqu'on s'aventure un peu vers l'inconnu. Grâce à mes études d'architecture, j'ai eu l'occasion de partir pour un semestre à Miami où j'ai pu approfondir mes connaissances en architecture durable et écologique.
Voyager est pour moi une manière incroyable de s'ouvrir au monde, de se challenger et d'en apprendre toujours plus. 
Cette année j'ai également eu la possibilité d'effectuer un stage dans le bureau Degré47 qui favorise l'auto-construction et le circuit court. Ce qui m'a énormement appris en architecture de terrain ainsi qu'en gestion de chantier. 

![](docs/images/trio.jpg)

## Mon parcours

Si j'avai pu retarder pendant un an le moment fatidique du choix des études que j'allais devoir commencencer, une fois revenue des USA, il était temps... Si l'architecture ne s'imposait pas comme un choix de coeur, il avait néanmoins le mérite d'être un choix réfléchi et qui cochait un bon nombre de cases. J'ai donc commencé ma première en 2016 et me voilà 4 ans plus tard, en train de terminer ma dernière. Après ces quelques années d'étude, j'ai enfin pu trouver un sens à ce que je faisais et surtout j'ai pu comprendre ce qui me passionait le plus. Ce n'est pas le dessin des plans ni la concpetion même d'un projet qui me stimule. J'ai besoin de voir du terrain, d'être en action en permanence, mais au-delà de ça, je sens qu'aider les gens est ce qui me fera me lever chaque matin pour une bonne partie de ma vie. Après mon erasmus à Miami, j'ai eu l'occasion de partir faire un mois de bénévolat à Porto Rico afin d'aider la population locale à se relever après les ouragans Irma et Maria qui ont frappé l'île deux ans auparavant. Ces quatre semaines ont pour moi été une révélation et m'ont convaincue dans le fait que je voulais me spécialiser en architecture de terrain et plus particulièrement en architecture d'urgence. C'est pour quoi, si tout se passe bien, l'année prochaine, je commencerai un master complémentaire en architecture d'urgence à Barcelone. 
                       


## Première rencontre avec l'objet qui rythmera mon semestre...

![](docs/images/sacco.jpg)

Le cheminement dans la conception du projet final a tout d'abord commencé par la visite du musée ADAM et de sa collection _The Plastic Collection_ par A. Bonny, A. Midal et R. Thommeret. Lors de la visite, il nous a été demandé de choisir un élement de mobilier parmi tous ceux présentés dans les salles d'exposition. Une fois notre choix fait, il nous fallait expliquer notre choix et sur quelle émotions/bases/raisons nous nous étions appuyées pour le faire. 
J'ai donc choisi le fauteuil du trio Gatti, Paolini, Teodoro appelé "Fauteuil". 
Ce choix s'est imposé très simplement. Le fauteuil dénotait de par ses différences dans cette salle d’exposition pleine d’objets aux arrêtes précises et aux couleurs vives. Il n’était ni beau ni moche mais semblait être d’un confort incroyable et prêt à accueillir quiconque qui voulait faire une petite pause. Contrairement aux autres pièces de mobilier de la salle, il n’était pas en quête de s’afficher comme « la » pièce spectaculaire mais se déposait tout à fait modestement parmi un arc-en-ciel de couleurs plus vives les unes que les autres. Fabriqué en PVC, il faisait partie intégrante de cette exposition dédiée aux éléments plastiques mais là encore il parvient à se différencier par un rendu matériel qui s’approche presque du cuir et qui lui confère un sentiment de chaleur et d’accueil. Bien différent de la sensation de rigidité et d’inconfort que les autres éléments de mobilier me transmettaient.
Alors qu’en français l’objet s’appelle « Fauteuil », c’est son appellation italienne qui le décrit le mieux selon moi. « Sacco » , qui signifie sac en français, nous rappelle ce qu’on désigne plus communément comme un « pouf ».
