# Final Project

# L'objet qui a animé mon semestre...


Le cheminement dans la conception du projet final a tout d'abord commencé par la visite du musée ADAM et de sa collection _The Plastic Collection_ par A. Bonny, A. Midal et R. Thommeret. Lors de la visite, il nous a été demandé de choisir un élement de mobilier parmi tous ceux présentés dans les salles d'exposition. Une fois notre choix fait, il nous fallait expliquer notre choix et sur quelle émotions/bases/raisons nous nous étions appuyées pour le faire. 
J'ai donc choisi le fauteuil du trio Gatti, Paolini, Teodoro appelé "Fauteuil". 

![](docs/images/pouf-200.jpg)

## Pourquoi lui et pas un autre?

Ce choix s'est imposé très simplement. Le fauteuil dénotait de par ses différences dans cette salle d’exposition pleine d’objets aux arrêtes précises et aux couleurs vives. Il n’était ni beau ni moche mais semblait être d’un confort incroyable et prêt à accueillir quiconque qui voulait faire une petite pause. Contrairement aux autres pièces de mobilier de la salle, il n’était pas en quête de s’afficher comme « la » pièce spectaculaire mais se déposait tout à fait modestement parmi un arc-en-ciel de couleurs plus vives les unes que les autres. Fabriqué en PVC, il faisait partie intégrante de cette exposition dédiée aux éléments plastiques mais là encore il parvient à se différencier par un rendu matériel qui s’approche presque du cuir et qui lui confère un sentiment de chaleur et d’accueil. Bien différent de la sensation de rigidité et d’inconfort que les autres éléments de mobilier me transmettaient.
Alors qu’en français l’objet s’appelle « Fauteuil », c’est son appellation italienne qui le décrit le mieux selon moi. « Sacco » , qui signifie sac en français, nous rappelle ce qu’on désigne plus communément comme un « pouf ».


# Objet final, son cheminement...

## Etape préliminaire de réflexion

Pour cette dernière phase du semestre, il nous est presque laissé carte blanche quant à l’objet final que l’on souhaite réaliser mais cinq termes nous ont tout de même été proposés afin de guider notre travail. Ils vont nous permettre de choisir un chemin de réflexion et de travailler notre idée tout en gardant en tête notre objet de base. 

- Référence
- Influence
- Hommage
- Inspiration
- Extension

Ces cinq mots nous ont été expliqués dans un premier temps au travers des définitions que l’on peut trouver dans les dictionnaires. Ensuite il nous a été demandé d’en choisir un, celui qui nous parlait le plus, qui nous intéressait le plus, et de le redéfinir selon notre propre ressenti et notre définition. Le choix n’a pas été facile au départ car bien que les termes Référence, Hommage et Extension me semblaient clairs et distincts, Influence et Inspiration apparaissaient encore comme très flous et je ne parvenais pas à percevoir les différences qui rendaient ces deux termes uniques. J’en ai donc discuté avec mon entourage afin de voir comment d’autres pouvaient percevoir ces deux mots. Quant influence pouvait prendre un aspect négatif, inspiration lui ne faisait jaillir dans le débat que des ressentis positifs. J'ai donc opté pour ce dernier et j'en ai recherché ses définitions afin d'avoir tous les outils en ma possession pour l'appréhender au mieux.

## Inspiration : ses définitions

**Larousse**

- Mouvement intérieur, impulsion qui porte à faire, à suggérer ou à conseiller quelque action : Suivre son inspiration.

- Enthousiasme, souffle créateur qui anime l'écrivain, l'artiste, le chercheur:Chercher l'inspiration.

- Ce qui est ainsi inspiré : De géniales inspirations.

- Influence exercée sur un auteur, sur une œuvre : Une décoration d'inspiration crétoise.

**Le Petit Robert**

- DIDACTIQUE Souffle émanant d'un être surnaturel, qui apporterait aux hommes des révélations. L'inspiration du Saint-Esprit.
- Souffle créateur qui anime les artistes, les chercheurs. L'inspiration poétique. Attendre l'inspiration.
- (ŒUVRE, ART) D'inspiration (+ ADJECTIF), qui s'inspire de (une œuvre du passé…). Une musique d'inspiration romantique.
- Idée, résolution spontanée, soudaine. Une heureuse inspiration.

**CNRTL**

_Domaine religieux_:
- Incitation, impulsion d'origine divine ou surnaturelle. Être animé, averti de qqc., guidé par une inspiration céleste, divine; avoir une inspiration du ciel; faire qqc. sous l'inspiration de Dieu
- État mystique dans lequel le croyant reçoit de Dieu la révélation de ce qu'il doit faire, dire, penser. On considère le merveilleux tableau qu'elle renferme comme un nouveau reflet de la sainte inspiration de l’héroïne

_Domaine littéraire_:
- Souffle créateur qui anime l'écrivain, l'artiste ou le poète.
- Influence exercée sur un artiste, un auteur, une œuvre.

_Domaine psychologique_:
- Idée subite, spontanée, intuition qui pousse à agir d'une certaine façon. Je vous préviens qu'il vient de me survenir à l'instant une inspiration très lumineuse
- Impulsion, mouvement intérieur qui pousse à agir. Sa bouche, qui criait et riait amèrement sous l'inspiration de la colère, resta froide et silencieuse pour répondre à des mots d’amour

_Domaine physiologique_:
- Action de faire pénétrer de l'air dans ses poumons; phase de la respiration pendant laquelle l'air pénètre dans les poumons

## Raisons du choix

Si dans un premier temps, j’ai choisi le terme Inspiration car il m’émoustillait les neurones de manière positive, il n’en est pas moins vrai qu’il fait également écho à l’histoire de la création du Sacco. Les créateurs du Sacco ont dit: “In the end we thought about the old mattresses stuffed with chestnut leaves, widely used by peasants: You take a sack, you fill it with leaves or similar materials, and this molds itself to fit the body”. La création du Sacco est donc elle même passée dans un premier temps par la phase de l’inspiration. Les créateurs se sont « inspirés » de cet objet de la vie quotidienne qu’est le matelas pour réfléchir ce nouveau fauteuil qui deviendra un symbole de design des années 60. Utiliser cette fois-ci le Sacco comme « objet qui inspire » est une belle manière de continuer sur cette voie tracée par les trois Turinois et d’imaginer l’objet que je souhaite réaliser.

