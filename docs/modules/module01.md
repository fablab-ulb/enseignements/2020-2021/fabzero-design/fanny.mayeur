# 1. Module 1 - Documentation

Pour la première semaine de formation, nous avons été inités au logiciel Fusion 360 dans un premier temps et ensuite à l'inteface Gitlab. La formation fusion nous servira notamment à la création de modèles 3D qui seront ensuite imprimés par les machines 3D. L'interface Gitlab quant à lui est un logiciel de partage de données et de connaissances ouvert à tous. 

# 2. Fusion 360

Nous avons eu comme première formation celle sur Fusion 360. Pour beaucoup d'entre nous il s'agissait là d'une première rencontre avec ce logiciel de conception et de rendu 3D, il était donc opportun de nous en expliquer les bases. Vous retrouverez le cheminement étape par étape de la réalisation de ma première impression 3D dans le module 2.

# 3. GitLab

Cette semaine je me suis familiarisée avec l'interface gitlab.com. Le but étant de me rendre plus autonome lors de l'utilisation de celui-ci et de rendre son accès plus didactique. Ce n'était pas gagné, et ça ne l'est à vrai dire toujours pas, mais au plus je me balade sur le site, au plus je commence à comprendre son mode de fonctionnement. Pour mieux comprendre son fonctionnement il a fallu que je me renseigne un peu plus sur le codage et sur le site Git en lui-même. Git est un logiciel libre qui permet l'édition de texte de manière collaborative entre différentes personnes et tout ça grâce au fait qu'il s'agit d'un site en opensource (logiciel dont la licence est en libre distribution). Gitlab fonctionne sous le système de Markdown. Alors c'est quoi exactement cette histoire de markdown? C'est ce que j'ai essayé de comprendre ensuite...

![](docs/images/lab.jpeg)

## Markdown

Markdoxn est un langage de balisage d'édition de texte internet tout comme HTML ou encore LaTex. A la différence de ces deux derniers exemples, Markdown se veut beaucoup plus intuitif et ouvert au grand public. Comme vous pouvez le voir sur ces captures d'écran, les différentes manières de mettre en gras du texte via un éditeur de texte. Markdown cherche vraiment à simplifier les chose.

![](docs/images/grasFF.jpg)

Avec ce nouveau langage, ses créateurs souhaitaient simplifier l'édition de texte pour des gens qui ne sont pas forcémenent des as en informatique mais qui dans leur usage journalier ont besoin de rédiger des textes que ce soit pour des publicités, sites web ou autres textes internet. 
Au départ le principe de codage, et tout ce qui lui est associé, me semblait totalement hors de portée mais après avoir regardé cette [vidéo](https://www.youtube.com/watch?v=f49LJV1i-_w&ab_channel=Codecademy) j'ai pu comprendre un peu mieux. 
J'ai aussi trouvé pas mal d'informations générales sur ce [site](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/markdown/). Il reprend énormément d'exemples de commandes et leur utilisation dans Markdown. 
Et en fait pour tout vous dire, il a fallut comprendre assez rapidement comment ce système d'édition de textes fonctionnait puisque c'est ce qui me permet de vous transmettre toutes ces informations ;) 

## Télécharger Gitlab directement sur son ordinateur

Malheureusement par manque d'espace sur mon ordinateur et à cause d'une vieille version et de l'impossibilité de faire de mise à jour, je n'ai pas pu télécharger Gitlab directement sur mon ordinateur. Pour complèter toute ma documentation je suis donc passée par l'interface internet. Mais je conseille à tout le monde, s'il en a l'occasion, de télécharger Gitlab sur son ordinateur car il permet d'éditer plus rapidement. Ce [site](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git) vous explique étape par étape le chemin qu'il faut suivre pour télécharger gitlab mais je vous résume ici les grandes étapes:  
- Tout d'abord il faut se rendre sur le [site](https://git-scm.com/downloads) et télécharger la version qui correspond au type d'ordinateur que vous avez. 

![](docs/images/git_ins.jpg)

- Une fois téléchargée, il se peut que vous deviez autoriser l'accès à cette application qui provient d'internet. Pour se faire, aller dans les paramètres de l'oridnateur et acceptez l'utilisation de l'application. 
- Gitlab est donc installé et il va falloir maintenant le configurer et ajouter vos références. Pour cela, il va falloir encoder le nom d'utilisateur que vous allez utiliser pour vous identifier comme auteur. Il faudra également ajouter une adresse email. Il faut absolument que votre nom et adresse email correspondent à ceux indiquer dans Gitlab (internet). 
- Enfin il faut relier votre compte Gitlab internet à celui utiliser sur votre ordinateur. Il y a deux moyens pour y arriver. Le premier est de s'identifier à chaque nouvelle connexion via HTTPS avec vos références. Le deuxième propose une solution sur la longueur et vous évitera de passer par l'étape "connexion" à chaque fois. Il s'agira donc de créer une clé SSH. 

## Clé SSH
Comme pour le téléchargement de Gitlab sur l'ordinateur, ce [site](https://docs.gitlab.com/ee/ssh/README.html) reprend toutes les étapes. 
Je vous les résume tout de même ici:
- Générer une clé SSH. Il en existe deux: la clé ED25519 (considérée comme plus sécurisée et performante) ou la clé RSA. 
- Une fois générée, deux clés reliées sont créées, une publique et une privée. Il faudra donc juste associée la clé publique à votre profil Gitlab en ligne et la clé privée directement à celui sur votre ordinateur. 


## TIPS pour l'utilisation de Markdown sur Gitlab

1. **Texte**

Pour écrire sur Gitlab en ligne, c'est très simple! Il suffit d'aller dans le module/chapitre que vous souhaitez éditer, clicker sur **Edit** et vous êtes prêts pour commencer votre texte. A tous moments, le bouton **Preview**, situé en haut à gauche de la zone d'édition, vous permet d'avoir un aperçu de votre travail avant d'enregistrer les modifications avec le bouton vert **Commit changes** qui lui se trouve en bas à gauche et qui enregistrera la nouvelle version de votre travail. 

2. **Images**

Pour agrémenter vos textes, il est possible d'ajouter des images. Pour se faire, vous pouvez créer un dossier images dans l'onglet docs et y télécharger vos images (en format jpeg) en appuyant sur **Upload**. Une fois la photo téléchargée, un lien sera généré et correspondera à cette image. Il s'agira donc ensuite de coller ce lien dans la formule  _![](lien image)_  à l'endroit où vous souhaitez voir l'image apparaitre et le tour sera joué. Attention tout de même au poids des images que vous téléchargerez. Trop lourdes elles risquent de ralentir le site, trop légères elles pourraient ne pas bien s'afficher. 

_Réduire le poids d'une image_

_J'ai tenté de télécharger l'extension _Graphicsmagick_. Mais avant ça, il s'agissait de comprendre comment télécharger ce logiciel, j'ai donc regardé une vidéo et plusieurs sites web:_
[](https://www.youtube.com/watch?v=pOEC4IowBjU)
[](http://www.graphicsmagick.org/)
[](http://www.graphicsmagick.org/README.html)

_Malheureusment, et malgré ces multiples aides, je ne suis pas parvenue à comprendre le fonctionnement de Graphicsmagick. J'ai donc opté pour la solution Photoshop et j'ai pu réduire la taille de la photo suivant ce procédé:_ 
- _Ouvrir la copie de l'image dans le logiciel de retouche photo (Photoshop)_ 
- _Aller dans le menu Image, cherchez la fonction Taille de l'image ou Redimensionner._ 
- _Vérifier que la case Conserver les proportions est bien cochée et saisir la nouvelle largeur de l'image, en pixels. La hauteur s'ajustera automatiquement_

_Grâce à cette technique, la taille de l'image a été réduite de 3072 × 4608 (1.8Mo) à 200 x 300 (17Ko) et j'ai pu la téléchargée sur ma page de présentation personelle._

3. **Historique des modifications**

Le principe même de Gitlab et création de textes de manière collaborative. Pour simplifier la communication entre les différents auteurs, Gitlab a mis en place une fonctionnalité qui permet de notifier les modifications apportées au texte via des messages répertoriés dans un historique. Il suffit pour ça d'encoder ce message dans la case qui se trouve en bas de page appelée _commit message_. 

4. **Commandes Markdown**

Je vous conseille vivement ce [site](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) pour trouver toutes les commandes nécéssaires à la bonne mise en page de votre texte. En voici déjà quelques exemples: 
![](docs/images/Commandes_listes.png)

