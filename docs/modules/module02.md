# 2. Module 2 - Fusion 360

Dans ce module 2 il nous est demandé de nous familiariser avec le logiciel Fusion 360, qui nous permettra par la suite d'imprimer en 3D nos réalisations. Fusion 360 est un logiciel CAO développé par Autodesk. Il permet la réalisation d'objet dans toutes les étapes de développement. 
Pour nous présenter ce nouveau logiciel, on a eu une formation avec Thibault. Il a fait le tour des fonctionnalités principales proposées par fusion 360 au moyen de la création de deux objets ( le cube du Gablab et une pièce d'échec). Après cette formation, j'ai regardé cette [vidéo](https://www.youtube.com/watch?v=ExHGnBdvz7o) afin de me familiariser encore plus avec le logiciel. 

Une fois la phase d'introduction du logiciel terminée, il nous était demandé de modèliser sur fusion 360 l'élement de mobilier que nous avions choisi au musée. J'ai choisi le **Sacco des designers italiens P. Gatti, C. Paolini et F. Teodoro**. Me voilà donc sur le point de modèliser ce fauteuil aux formes naturelles, sans angle, se laissant aller à la forme de la personne qui s'y assi, pas une mince affaire, mais je relève la défi! 

# Etude de l'objet

Pour pouvoir le modèliser correctement il fallait tout d'abord que je m'intéresse à son histoire, ses dimensions, sa matérialité... J'ai donc fait quelques recherches lors desquelles j'ai pu me rendre compte de la place de ce fauteuil dans le monde du design ainsi que de l'impact qu'il a eu au moment de sa création en 1968. Il s'agissait là d'un fauteuil innovant. Sans structure de support et grâce à sa forme instable, il permettait une adaptation naturelle à la position de l'utilisateur, en prenant différentes formes.

## Dimensions et matérialité

Le Sacco n'a pas de forme précise, car il n'a pas de structure interne, c'est dimensions sont donc purement pratiques et utiles qu'au moment de la conception. J'ai pu constaté au court de mes recheches que le sacco avait été créé dans plusieurs tailles. Celles reprises ci-dessous sont celles d'origine.
- Hauteur théorique: ~ 140cm
- Hauteur réelle: ~ 100cm
- Diamètre de la base: 80cm

![](images/dis_saccp.jpg)

Les lignes et la forme du sacco dépendront donc entièrement de la personne qui l'utilisera. Le rembourrage du fauteuil est fait de billes de polystyrène expansé à haute résistance. Le revêtement extérieur monochrome est quant à lui en tissu ou en cuir. Une fermeture éclair a été insérée à la base du siège pour permettre de changer le rembourrage si besoin. 

![](images/pub_sacco.jpg)


# Modélisation de l'objet

## Apprentissage supplémentaire de Fusion 360

Le sacco ayant une forme très particulière et des courbes naturelles j'ai du dans un premier temps regardé énormément de tutotoriels m'expliquant les différentes manières pour créer des objets aux formes organiques. Ces trois vidéos sont celles qui m'ont le plus appris dans ce domaine-là: 

- [Créer un vase aux formes courbes](https://www.youtube.com/watch?v=_rqwZewYoJQ) 
- [Créer une forme organique](https://www.youtube.com/watch?v=DxYTjJCqwNQ) 
- [Introduction à la création et à la scultpure de formes](https://www.youtube.com/watch?v=UaNS8V9S68w)

Une fois que j'avais, plus ou moins, les outils en mains pour créer de telles formes, il était temps que je me lance sur Fusion 360. Mon cheminement a donc fonctionné sous le principe d'essais-erreur jusqu'à arriver à une forme qui me semblait la plus proche de la réalité. 


## Le développement de l'objet

**1. Première technique:**

Pour mon premier essai j'ai donc suivi les conseils d'une des vidéos que j'avais regardées. 
J'ai tout d'abord dessiné sur un premier plan (au niveau du sol) une esquisse de forme circulaire. Ensuite j'ai ajouté un créé un deuxième plan grâce à l'outil "plan de décalage", que j'ai disposé à une hauteur de 140cm pour dessiner le Sacco en 1:1. J'ai dessiné sur ce deuxième plan à nouveau une esquisse circulaire mais ici de plus petite taille. Une fois ces deux dessins créés, j'ai poursuivi en utilisant l'outil "Lissage" qui permet de relier deux esquisses et d'ainsi générer une volume. Satisfaite que tout fonctionne correctement, je me rendais bien compte que j'étais encore bien loin de la réelle forme de mon fauteuil.

![](images/technique_1.jpg)

J'ai donc tenté d'approfondir cette technique en créant plusieurs plans de décalage et donc plusieurs esquisses que je pourrais relier entre elle avec l'outil "Lissag". Et pour complexifier un peu le volume et lui donner des courbes plus naturelles, je ne me suis plus contenté de simples cercles mais à l'aide de l'outil "Spline" j'ai créé des formes différentes les unes des autres. Le résultat était nettement plus satisfaisant mais je n'y étais pas encore. Le fauteuil paraissait encore trop rigide et son allure générale ne ressemblait pas du tout au véritable Sacco. 

![](images/techniqu_2.jpg)

**2. Deuxième technique:**

Pour cette seconde tentative je me suis lancée sur un système plus "naturel" et plus modulable que lors de l'essai précédent. 
Je suis donc tout d'abord partie de l'outils "Créer une forme" depuis lequel j'ai généré une boîte. Afin de pouvoir plus facilement la moduler et la transformer j'ai diviser la surface de la boîte en plus de parties que proposé initialement. En créant plus de lignes de manipulation de la sorte, je m'assurais une plus grand liberté dans les modifications que j'apporterai à la forme. Pour ensuite pouvoir jouer avec ces lignes de manipulation, j'ai cliqué sur l'outil "Modifier". Des flèches de directions sont apparues me permettant de modifier la position des points/lignes/faces que je sélectionnais sur le volume de base. 

![](images/techn3.jpg)

Si je sentais que cette technique avait du potentiel, je constatais bien que je n'avais pas encore le Sacco devant mes yeux. Il a donc fallu que je continue à jouer sur ce modèle pendant quelques temps pour parvenir à la forme finale. Il n'y avait là que la persévérance qui pouvait me faire parvenir à un résultat qui acceptable... 

![](images/tech_4.jpg)


## Bonus

Une fois la forme générale créée, j'ai voulu aller un peu plus loin et proposer des plug-in pour le Sacco. Je vous en dis plus dans le module 3 si ça vous intéresse, mais je vous mets déjà un petit aperçu ici ;) 

![](images/plugin.jpg)


J'ai par la même occasion essayé d'utiliser l'option Render dans Fusion 360 et je dois dire qu'il y a encore du chemin à faire, c'est pas encore au top...

