# Module 3 - Impression 3D

Cette nouvelle semaine de formation a été consacrée à l’impression 3D. Après la réalisation de nos modèles sur Fusion 360 il était donc temps de les concrétiser et de les rendre réels. Il s’agissait là de ma première utilisation d'imprimante 3D, et je dois dire que c’est vraiment sympa de voir se créer en quelques heures et à l’aide de ces filaments de couleur fondus, le petit objet qu’on aura mis du temps à modéliser.

# 1. La machine et ses spécificités

Au Fablab, ce sont des imprimantes Original Prusa I3 MK3S qui sont utilisées. Considérées comme les meilleures des imprimantes Prusa, elles ont également la particularité d’être disponibles en Opensource. Cela signifie que n’importe qui peut récupérer le fichier-mère de construction de l’imprimante et l’utiliser pour créer sa propre imprimante. Le twist sympa qu’il y a là, c’est surtout que ces machines sont imprimées à partir d’autres imprimantes Prusa. En imaginant une telle caractéristique, la marque souhaitait réduire le coût de fabrication, permettre une production automatique mais aussi faciliter le remplacement des pièces défectueuses si besoin. Toutes les pièces étant imprimées par d’autres imprimantes, il est très facile de les remplacer lorsque celles-ci sont cassées. 

![](docs/images/prusa_ima.jpg)


Le fonctionnement de l’imprimante est très simple. Une bobine de filament est reliée à la tête d’impression dans laquelle le fil est monté à haute température afin d’être malléable et de pouvoir être  déposé sur le plateau d’impression suivant la forme du modèle. Il y existe une grande variété de filaments proposée pour cette machine, allant de couleurs sobres au plus peps et tout en étant disponibles à partir de matériaux divers. Ici au fablab de l'ULB, on nous propose essentiellement du PLA. Il est également possible d’utiliser du filament fait à partir de sciure de bois mais contrairement au PLA, c'est un matériau assez difficile à manier. 

Pour utiliser la machine, il s'agit dans un premier temps de programmer les paramètres qui dicteront l’impression. Pour se faire, il faut télécharger le logiciel Prusa slicer correspondant à l’imprimante utilisée, dans ce cas-ci la Prusa I3 MK3S. Disponible [ici](https://www.prusa3d.com/drivers/) sur le site internet de Prusa. Le logiciel est très facile à télécharger et ne prend pas énormément d’espace de stockage sur l’ordinateur.

## Résumé des étapes à suivre pour l'impression d'un modèle

- Enregistrer au format STL le fichier créé sur Fusion 360
- Ouvrir le fichier dans le logiciel Prusa Slicer pour paramètrer l'impression 
- Une fois les réglages terminés, exporter le fichier en G-code 
- Copier ce G-code sur la carte SD de l'imprimante
- Vérifier que l'imprimante est en état de marche
- Choisir le fichier à imprimer
- Lancer l'impression 

Et le tour est joué! 

# 2. Prusa Slicer - Paramétrage étape par étape

_Comme expliqué à la fin du module 2, il est impératif d’enregistrer le modèle (créé à partir du logiciel de création 3D de votre choix) au format STL. Il s’agit là du seul format reconnu par l’imprimante. Une fois enregistré au bon format, il peut être ouvert dans le logiciel Prusa en le faisant glissé tout simplement. Il apparaitra sur un plateau semblable au plateau d’impression de la machine. C’est à ce moment là qu’on peut commencer à paramétrer l’impression._ 

**1. Positionnement de l'objet** 

![](docs/images/prusa_im.jpg)

Il faut choisir la face de stabilisation adéquate pour permettre une impression facile. A entendre par là, qu’il faut choisir la position de l’objet qui permet d’avoir la plus grande surface de contact avec le plateau tout en évitant d’engendrer une trop grande quantité de supports qui seront générer pour les portes-a-faux ou autres subtilités du modèle. Plusieurs commandes sont à notre disposition pour gérer la disposition. Deplacer, rotation, échelle… Les dimensions du plateau sont 20x20x22. Il est donc important de régler l’échelle de l’objet en gardant cette donnée en tête.

Ensuite il faut procéder aux réglages plus « techniques » de la machine. Trois modes sont proposés pour cette étapes. Pour accéder à la totalité des réglages, il faut se mettre en mode expert, ce que j’ai fait. 

**2. Couches et périmètre**

Dans ce réglage, il est question de Hauteur de couche et de parois verticales et de coques horizontale. EN ce qui concerne la hauteur de couche, elle dépendra de la tête d’impression de la machine utilisée et sera toujours égale à la taille de celle-ci divisé par deux (ex: si la tête = 0,4mm, il faudra inscrire pour la hauteur de couche 0,2mm). Pour les parois verticales et les coques horizontales, je pensais au départ qu’il s’agissait de reglages totalement distinct mais les fabmanageuses m’ont expliqué que suite à une traduction de l’anglais malheureuse, coques horizontale signifie au final Parois horizontale. Elles m’ont donc conseillé de toujours mettre les mêmes valeurs pour les deux types de parois afin de ne pas risquer d’avoir des parois plus faibles que d’autres et donc une déformation de l’objet. J’ai donc opté pour 3mm afin de garantir une certaine solidité. Les autres réglages de cette section peuvent rester par défaut.


![](docs/images/couche_f.jpg)


**3. Remplissage**

En créant notre objet à imprimer, inévitablement de la matière perdue se trouve à l’intérieur du la « coque » extérieure. Cette matière invisible peut être gérée sur ce logiciel afin de rendre l’objet plus ou moins résistant aux pressions extérieures qui pourraient ^être appliquées. Il va de soi qu’au plus on rempli notre objet de matière, au plus il sera résistant. Mais il n’est pas toujours, si pas jamais, n’écossaire de prévoir un remplissage à 100% de densité. Les motifs de remplissages qui sont proposés sont imaginés pour contrer des forces venant de toutes direction, remplir la pièce conviendrait donc à du gachi de matière. Giroide, nid d’abeille, rectangle, il en existe énormément. A retenir que le grioide est la forme la plus solide. Grace à ces motifs on peut donc se permettre de réduire la densité de remplissage à des valeurs entre 10 et 15%, et ce sera amplement suffisant. Au-delà de 35%, on considèrere vraiment cela comme du gaspillage de matière donc il faut vraiment veiller à régler ce paramètre correctement. Le reste de parametres reste par défaut.

![](docs/images/rempl_f.jpg)


**4. Jupe et bordures**

Pour garantir une imposesion parfaite il faut veiller à ce que l’objet ne bouge pas pendant l’impression, c’est pour cela qu’ont été inventé les jupes et bordure. La jupe va se programmer par défaut pour assurer la stabilité générale de l’objet. Mais au plus celui-ci sera haut ou aura des portes-a-faux, au plus il faudra augmenter l’emprise au sol sur le plateau et c’est la que la gestion des bordures est importante. Dans mon cas, je n’ai touché à rien car mon objet ne présentait pas de risques particuliers.

![](docs/images/jupe_f.jpg)


**5. Supports**

Il s’agit peut-être là d’un des règlages les plus importants. Ces supports fonctionnent comme des petits échafaudages chargées de supporter les parties en port-afaux du modèle. En effet la machine ne peut pas imprimer dans le vide donc ils agissent comme une extension du sol pour permettre une impression efficace. Ces supports peuvent etre générés automatiquement par le logiciel. C’est à ce moment-là que le choix de la face de stabilisation de l’objet sur le plateau  est important, il permet d’éviter de générer des supports inutilement. Une petite astuce supplémentaire pour éviter de se retrouver avec des supports inutiles, c’est de cocher « Support sur le plateau uniquement ». Dans mon cas, quelques supports ont du etre générés pour la poignée du sacco et pour les accroches de la poches à livres.

![](docs/images/suuport_f.jpg)

**6. Conseil personnel**

Lors des réglages d’impression, il faut faire très attention à la taille de la tête d’impression de la machine qui sera utilisée. Au fablabs deux tailles sont disponibles: 0,4mm et 0,6mm. Cela aura un impact sur les réglages de la hauteur de couche mais également sur les réglages de l’imprimante (reglage imprimante -> Extudeurs -> diamètre de la buse). Il faudra donc veiller à encoder correctement ces modifications s’il y a une variation dans la taille de la buse.

![](docs/images/bobine.jpg)

## Etape finale sur Prusa Slicer

Une fois tous ces réglages programmés il faut exporter le fichier en G-code. Une fenêtre d’informations apparaitra et vous indiquera le temps d’impression, la quantité de matière qui ser utilisé et le coût que cela engendrera.



# 3. Imprimante I3 MK3S

Après avoir créé le fichier, je l'ai enregistré à mon nom et déposé sur la carte SD assignée à l’imprimante que  j’allais utiliser. 

La suite du processus se passe à présent sur l’imprimante elle-même. J’ai dans un premier temps vérifié que la machine était en bon état et j’ai nettoyé le plateau à l’acetone pour éviter qu’il n’y ait des résidus d’impressions précédentes sur le plateau qui risqueraient d’altérer mon impression. 

Je souhaitais aussi changer la bobine de filament pour une autre couleur. Pour faire cela, il faut aller dans les réglages du filament, cliquer sur « Unload the filament ». La machine va commencer à chauffer le filament pour que les résidus à l’intérieur de la buse soient expulsés et une fois que c’est fait un message apparait sur l’écran indiquant qu’on peut retirer, **délicatement**, le fil de la buse. Pour insérer le nouveau filament, il suffit de cliquer sur « Load the filament » dans le même menu que l’étape précednte et de glisser, **délicatement**, le nouveau fil dans la buse.


Après tous ces changements, la marche à suivre se passe sur l’écran de l’imprimante. Il faut tou d’abord cliquer sur « print from SD ». C’est dans ce menu qu’apparaissent les dossiers de la carte SD et j’y ai donc trouvé celui à mon nom. Une fois dans mon dossier personnel, j’ai choisi le fichier que je souhaitais imprimer (appelé ici "finalle.gcode"). En cliquant sur le bouton j’ai enfin lancé ma première impression. C’était parti! 

![](docs/images/impri_f.jpg)

A tous moments pendant l’impression il est possible de gérer quelques réglages d’impression comme la vitesse d’impression (speed), la température de la buse (nozzle) et la température du plateau (bed). Je n’ai touché à rien pour le coup. 

A la fin de l'impression, pour retirer l'objet du plateau d'impression, il suffit de déformer un peu le plateau pour que la pièce se décolle. Lorsqu'elle est libérée, il n'y a plus qu'à utiliser une pince pour retirer les suppports qui sont encore attachés à l'objet.


![](docs/images/tout_f.jpg)


## Faux départ...

L’impression était en cours lorsque je me suis aperçu que des restes de filament de la bobine précédente étaient restés collés à la buse et se répandaient sur mon impression. Si dans un premier temps j’ai réussi à sauver les meubles, quelques minutes plus tard, une énorme goutte est tombée en plein milieu du modèle et j’ai fait le choix d’arrêter l’impression et d’en relancer une nouvelle. Je ne voulais pas prendre le risque que cet amas de matière indésirable n’altère le reste de l’impression et ne gaspille du filament inutilement. J’ai donc changé de machine et ai relancé une nouvelle impression, qui s’est passée, elle sans encombre. 

![](docs/images/faux_dep.jpg)

# 4. Plug-in 

Lors de la modélisation 3D de mon objet j’ai souhaité ajouter quelques plug-in qui me semblaient adéquats à l’objet que j’étais en train de conceptualiser. 

Le sacco est un fauteuil qui se veut malléable et adaptable à tous les corps et à toutes les situations. En rajoutant une poignée, je souhaitais donc amplifier cette qualité qu’il a d’être transportable et donc en faciliter son déplacement pour l’utilisateur. 
Cette réflexion m’a ensuite menée plus loin et je me suis demandée ce à quoi était amené à faire l’utilisateur lorsqu’il est assis dans ce fauteuil. Peut-être lit-il? Est-il le nez dans son telephone? Ou encore, en train de dessiner sur sa tablette graphique. De là est néé l’idée d’ajouter un filet sur la base du sacco qui permettrait ainsi le rangement des ces quelques objets de la vie quotidienne.



![](docs/images/sacco_fin.jpg)


