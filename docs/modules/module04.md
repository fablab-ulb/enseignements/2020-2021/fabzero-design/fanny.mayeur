# Module 4 - Découpe assistée par ordinateur

Pour cette dernière semaine de formation, nous avons été initiés à la découpe laser autrement appelé Laser Cutter. Il s’agit là d’une machine qui permet de faire des découpes ou des gravures dans des matériaux divers, allant du bois ou plastique, à l’aide d’un laser. 

L’exercice était de partir de notre objet (le Sacco pour rappel) et de s’en inspirer pour créer une lampe. Nous devrons pour cela utiliser une feuille en polypropylène de 60x80 cm.

Le Fablab de l’ULB met à notre disposition deux machines laser. La Lasersaure et la Fullspectrum. Pour créer ma lampe j’ai utilisé la première car elle permet des découpes plus grandes et est plus puissante.

![](docs/images/machines.jpg)

# Lasersaure

## Fonctionnement général
Il s’agit là encore d’une machine en Opensource. Les dimensions de la surface de découpes sont de 122x61cm, sur une hauteur de 12cm. Le laser mis en place sur cette machine est un ltube CO2 infrarouge et il peut atteindre une puissance de 100W. En ce qui concerne la vitesse de découpe, même si la machine permet d’aller jusqu’à une vitesse de 6000mm/min, il est quasiment impossible de l’atteindre et en plus le résultat de la découpe risquerait d’être très médiocre.
Pour utiliser cette découpeuse, les fichiers doitent être en SVG ou en DFX (tout en sachant que les SVG fonctionnent mieux). Le logiciel relié à la machine s’appelle Drive BoardApp. Pas besoin de le télécharger sur notre ordinateur personnel par contre car le Fablab met à notre disposition un ordinateur directement relié à la machine. 

Pour que l’ouverture du fichier dans Drive BoardApp se fasse sans encombre, il est opportun de passer le fichier dans un premier temps dans Inkscape. C’est un logiciel gratuit (et téléchargeable sur ce site gratuitement) qui transforme les fichiers en dessin vectoriel. 

## Précautions

Lors de l’utilisation de cette machine, il y a quelques précautions à prendre. 

- Il est nécéssaire de bien **connaitre le matériau **qu’on s’apprête à découper car des poussières nocives peuvent se détacher au moment de la découpe ou le matériau peut être inflammable. 
- Il faut toujours impérativement **ouvrir la vanne d’air comprimé et l’extracteur de fumée**. A deux ils permettent l’extraction de la fumée générée au moment de la découpe. 
- Avant tout lancement de la machine il faut s’assurer qu’un **extincteur de C02** est à proximité. 
- Et enfin et surtout il faut absolument **repérer le bouton d’arrêt d’urgence** de la machine.


## Matériaux utilisés

Tous les matériaux ne sont pas bons à utiliser dans la machine de découpe laser. Ils peuvent être classés par catégorie.

**Recommandés: **

Bois contreplaqué de 3/4mm
Acrylique (Plexiglass)
Papier
Carton
Textile

**Déconseillés:**

MDF (fumée nocive)
ABS,PS (fond trop facilement + fumée nocive)
Polyétylène épais, PE, PET, PP (fond facilement)
Matière fibreuse : fibre de verre, fibre de carbone (poussières nocives)
Métaux (ne se découpe pas)

**Interdits:**

PVC (fumée acide)
Cuivre (réfléchit le laser qui serait donc redirigé vers la machine directement —> caratsrophe!)
Téflon (fumée très nocive)
Vinyle (peut contenir de la chlorine)
Résine (phénolique ou epoxy) (fumée très nocive)


## Déroulement des festivités

**1. Mise en marche de la machine**

Tourner le bouton rouge d’arrêt d’urgence dans le sens des flèches blanches. Ouvrir toutes les vannes et s’assurer qu’on respecte bien les précautions décrite plus haut. 
Ramener le laser à sa position initiale en appuyant sur le bouton « Run Homing Cycle ».

![](docs/images/etape_1.jpg)

**2. Installation du matériau**

Installer la planche qui va être découpée dans le coin supérieur gauche de préférence, car c’est de là que partira le laser. 
C’est aussi le moment d’ajuster la focale du laser en fonction du matériau qui va être découpé grâce à ces petits guides qui se placent en dessous de la tête. Dans mon cas j’ai placé le laser à 15mm. 

![](docs/images/guide.jpg)

**3. Driveboard**

Il s’agit maintenant d’apporter le fichier de découpe dans le logiciel relié à la machine.
Appuyer sur le bouton « Open » et choisir le fichier désiré. Il est très courant que des problèmes arrivent lors de cette étape. Le logiciel a tendance à regrouper les traits du dessin ce qui empêcherait la découpe. Il faut donc bien s’assurer d’avoir passé le fichier préalablement dans Inkscape. 

Pour vérifier que le dessin se fera bien sur la planche prévue à cet effet, il est possible de prévisualiser le parcours du laser en appuyant sur le symbole représentant deux flèches qui s’éloignent qui se trouve juste à coté du bouton « RUN » qui sera lui utilisé pour lancer la découpe.

Une fois que le fichier s’est correctement ouvert, il faut détecter les différents types de découpes désirés. Il va donc falloir associé une couleur à un tracé. A l’aide du bouton « + » on pourra choisir une couleur des notre dessin cela détectera tous les autres trais de la même couleur. Il faut ensuite déterminé la puissance ( %) et la vitesse de découpe (F) associées à ces traits là. Il est vivement conseillé de mettre les gravures en premier et ensuite les découpes pour éviter tout déplacement fortuit du matériau qui pourrait causer un mauvais résultat final. 

![](docs/images/db.jpg)

Dans mon cas je n’avais que des découpes à faire. J’ai donc laissé la puissance sur 1500 et j’ai programmé la vitesse à 46%. Si j’avais voulu faire des gravures et vu le matériau que j’utilisais, j’aurais laissé la même puissance mais j’aurais réduit la vitesse à 20%.

Lorsque tous les réglages sont encodés, le bouton « Statut » devrait être au vert. S’il est orange ou rouge, il est possible de cliquer dessus pour constater les problèmes survenus. Il peut tout autant s’agir d’une vanne qui n’est pas ouverte que du mauvais positionnement de la feuille à découper. 

Une fois ce bouton passé au vert, on appuie sur le bouton « Run » pour lancer la découpe. Il est possible de mettre en pause la découpe si l’on souhaite déplacer la feuille ou s’il l’on constate une annomalier. Sans oublier que s’il y a un problème majeur il y a toujours le bouton d’arrêt d’urgence.



# Recherches et brainstorming

J'ai dans un premier temps brainstormer sur les notions/sensations/idées que m'inspirait le Sacco. En rassemblant mes impressions, des esquisses de projet sont nées dans ma tête et j'ai pu utiliser cela comme première étape dans la création de ma lampe. J'ai donc tenté de retranscrire ça à l'aide de prototypes en papier. 

![](docs/images/protoype.jpg)

Très vite je me suis rendue compte que ça ne me plaisait pas... Il manquait quelque chose. Le sacco, en plus de la volupté et du confort, m'inspirait l'idée de mouvement. Que ce soit du fait qu'on puisse le déplacer très facilement ou du fait qu'il change de forme suivant l'utilisateur qui s'y installe, j'avais envie de retrouver ce sentiment dans ma lampe.

# Conception

Mon intention était de créer une lampe qui ferait paraitre le sentiment de mouvement et d'évolution. Même si consciente que le principe de la découpe laser soit de découper des matériaux au format 2D, il m'a semblé opportun, dans un premier temps, de repartir sur fusion pour y développer la forme générale que prendra ma lampe. 

J'ai passé là quelques heures avant de finalement trouvé la forme qui comblait parfaitement mes attentes. Il fallait maintenant que je transforme cela en un dessin 2D. Je suis donc repartie dans un processus de reflexion pour trouver la meilleure manière de faire. Très vite je suis tombée sur une idée qui m'a plue. Si le Sacco se compose d'une multitude le billes pour créer son rembourage, pourquoi ne créerais-je pas une multitude de formes qui formeraient un tout pour monter ma lampe?

![](docs/images/chemine.jpg)

J'ai donc commencé à découper cette forme 3D en tranches horizontales que j'imaginais pouvoir supperposer ensuite pour créer l'abat-jourt de ma lampe. Le processus a été long et fastidieux car malgré mes recheches je n'ai pas trouvé d'outil sur Fusion 360 permettant de diviser un objet en un nombre choisi de parts. J'ai donc du répéter le même processus 24 fois: Créer un nouveau plan, diviser la forme au niveau de ce plan, déplacer la tranche pour permettre la prochaine "découpe", encore et encore... 
Une fois mes 24 pièces disposées dans le bon ordre sur fusion, j'ai exporté le fichier au format DWG afin de pouvoir l'ouvrir sur Autocad, un logiciel de dessin 2D. 


Me voilà sur Autocad avec mes tranches de lampe. Il était temps que je réflechisse à la manière donc allait s'articuler ces formes et surtout comment j'allais les assembler. Je gardais en tête comme but principal que ma lampe renvoie ce sentiment de mouvement. Et pourquoi ces tranches ne s'articuleraient-elles pas le long d'un coeur cylindrique qui leur permettrait de rotater librement? Pour réaliser cela, j'ai percé un cercle dans chacune des tranches et j'ai créé 4 montants qui serviraient de structure centrale. Pour pouvoir faire tenir les tranches, j'ai prévu des redents sur toute la hauteur des montants dans lesquels viendraient se glisser les tranches. Sur l'autre hauteur des montants j'ai dessiné des encoches dans lesquelles je pourrai accrocher la guirlande lumineuse qui servira de source de lumière. 


![](docs/images/evolu_plan.jpg)


Une fois en possession du dessin 2D de ma lampe, j'ai suivi le processus décrit plus tôt dans ce module pour la découpe laser. Après quelques ennuis pour lancer la découpe mais grâce à l'aide des Fabmanageurs, tout a fini par bien sortir et il ne restait plus qu'à assembler le tout. 


![](docs/images/assembla.jpg)


Au cours de la confection, je me suis rendue compte que des forces extérieurs poussaient les montants les uns vers les autres et que la structure ne fonctionnait pas comme espérer. Pour résoudre ce problème j'ai utilisé le négatif des cercles intérieurs des tranches dans lesquels j'ai coupé au cutter des fentes. Je les ai ensuite placé à l'intérieur de la structure (photo 3) pour qu'ils retiennent les forces extérieurs et gardent les montants en place. 
L'assemblage n'a pas été facile parce que le plastique glissait énormement et donc à chaque fois que je tentais de mettre une nouvelle tranche, celle du dessous s'en allait. Mais avec un peu de persévérance, l'aide de petites mains supplémentaires et la création d'une semelle dans laquelle j'ai inséré les montants, j'ai pu faire tenir le tout. 

La dernière étape était donc d'insérer la guirlande par le dessous, dans le trou que j'ai prévu à cet effet dans le socle. 

# Résultat final

![](docs/images/lum.jpg)


Comme prévu, je voulais que cette lampe donne un effet de mouvement et qu'elle s'adapte à chaque nouvelle utilisation...


![](docs/images/ulti.jpg)







